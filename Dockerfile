FROM node:11.1-alpine as build
RUN apk update && apk add bash && npm config set registry http://registry.npmjs.org && apk add curl

WORKDIR /root
RUN touch .bashrc .bash-profile .zshrc .profile && curl -o- -L https://yarnpkg.com/install.sh | bash

ADD ./ /src/app/insights-frontend

WORKDIR /src/app/insights-frontend
RUN npm rebuild node-sass && npm run build
RUN export NODE_ENV=production


FROM nginx:latest
WORKDIR /src/app
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /src/app/insights-frontend/dist/ ./dist
COPY ./index.html /src/app/
