import {store} from '../store/store'

export function getNotificationClass (responseStatus) {
  let notificationClass = null;

  if (responseStatus === 200 || responseStatus === 201 || responseStatus === 202) {
    notificationClass = 'has-background-success'
  }

  if (responseStatus === 404) {
    notificationClass = 'has-background-danger'
  }

  // TODO: add cases for errors

  return notificationClass
}


export function showNotification(response) {
  if (typeof(response) === 'string') {
    store.commit('updateNotificationClass', 'has-background-info');
    store.commit('updateNotification', response);
  } else {

    let notificationClass = getNotificationClass(response.data.status);
    let notificationMessage = response.data.message;

    store.commit('updateNotificationClass', notificationClass);
    store.commit('updateNotification', notificationMessage);
  }

  hideNotification();
}


export function hideNotification() {
  setTimeout(function () {
    store.commit('updateNotification', null);
    store.commit('updateNotificationClass', '');
  }, 7500);

}
