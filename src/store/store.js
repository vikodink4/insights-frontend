import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex);


export const store = new Vuex.Store({
  state: {
    currentFolderList: [],
    currentInsightsList: [],
    isLoading: false,
    notificationClass: null,
    notification: false,
    isAuthorized: false,
    currentUser: null,
  },
  getters: {
    currentFolderListState: (state) => {
      return state.currentFolderList;
    },
    loadingState: (state) => {
      return state.isLoading
    },
    notification: (state) => {
      return state.notification
    },
    notificationClass: (state) => {
      return state.notificationClass
    },
    currentInsightsList: (state) => {
      return state.currentInsightsList
    },
    isAuthorized: (state) => {
      return state.isAuthorized
    },
    currentUser: (state) => {
      return state.currentUser
    }
  },
  mutations: {
    fillCurrentFolderList(state, folders) {
      state.currentFolderList = folders;
    },
    runLoading(state) {
      state.isLoading = true
    },
    stopLoading(state) {
      state.isLoading = false
    },
    runNotification(state) {
      state.notification = true
    },
    stopNotification(state) {
      state.isNotification = false
    },
    updateNotificationClass(state, cls) {
      state.notificationClass = cls;
    },
    updateNotification(state, msg) {
      state.notification = msg;
    },
    fillCurrentInsightsList(state, insights) {
      state.currentInsightsList = insights;
    },
    updateIsAuthorizedState(state, value) {
      state.isAuthorized = value;
    },
    updateCurrentUser (state, value) {
      state.currentUser = value;
    }
  },
  actions: {}
});
