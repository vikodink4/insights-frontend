import Folders from '../components/sections/handles/Folders.vue'
import Folder from '../components/sections/handles/Folder.vue'
import Main from '../components/sections/main/Main.vue'
import Insights from '../components/sections/handles/Insights.vue'
import Login from '../components/sections/auth/Login.vue'


const routes = [
  { path: '/handles/folders/', component: Folders, name: 'folders'},
  { path: '/handles/folders/:id', component: Folder, name: 'specificFolder'},
  { path: '/handles/folders/:id/insights', component: Insights, name: 'insights'},
  { path: '/', component: Main, name: 'main'},
  { path: '/login', component: Login, name: 'login'}
];

export default routes
