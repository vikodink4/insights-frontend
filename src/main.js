import 'bulma/css/bulma.css'
import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VModal from 'vue-js-modal'

import routes from './routes/routes'
import {store} from './store/store'

import {verifyToken} from './api/services'

import {showNotification} from './utils/utils'

Vue.use(VueRouter);
Vue.use(VModal, {dynamic: true, injectModalsContainer: true});

const router = new VueRouter({routes: routes, mode: 'history'});

router.beforeEach((to, from, next) => {
    let jwtToken = localStorage.getItem('token');

    if ('login' === to.name) {
      if ("null" !== jwtToken && null !== jwtToken){
        verifyToken(jwtToken)
          .then(r => {
            if (r.data.token === jwtToken) {
              store.commit('updateIsAuthorizedState', true);
              console.log('redirecting to main page');
              router.push({name: 'main'})
            }
          })
      } else {
        next();
      }
    } else {
      console.log('here')
      if ("null" !== jwtToken && null !== jwtToken) {
        console.log('asd')
        verifyToken(jwtToken)
          .then(r => {
            if (r.data.token === jwtToken) {
              console.log('all ok')
              store.commit('updateIsAuthorizedState', true);
              next();
            }
          })
          .catch(e => {
            localStorage.setItem('token', null);
            router.push({name: 'login'});
            showNotification(e.response.data.non_field_errors[0]);
          })
      } else {
        router.push({name: 'login'})
      }
    }
});

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
});
