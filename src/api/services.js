import axios from 'axios'
import {store} from '../store/store'

const localhostURL = process.env.NODE_ENV === 'production' ? '/api' : 'http://localhost:8000/api';

const apiService = axios.create({
  baseURL: localhostURL
});

apiService.interceptors.request.use(
  reqConfig => {
    console.log('making request');
    reqConfig.headers.authorization = `JWT ${localStorage.getItem('token')}`;

    return reqConfig;
  },
  err => Promise.reject(err),
);

apiService.interceptors.response.use(
  response => response,
  error => {
      const {status} = error.response;
      if (status === 401) {
        store.commit('updateIsAuthorized', false);
        window.location.href = '/login'
      }
      return Promise.reject(error);
 }
);

// const localhostURL = 'http://cc4de2b6.ngrok.io';

// Auth
export function login(login, password) {
  return apiService.post('/api-token-auth/', {
    username: login,
    password: password
  })
}

export function verifyToken(token) {
  return apiService.post('/api-token-verify/', {
    token: token
  })
}

// Folders actions
export function getFolders() {
  return apiService.get('/folders/')}
export function getFolder(id) {return apiService.get(`/folders/${id}`)}
export function createFolder(folderName) {
  return apiService.post('/folders/', {
    data: {
      name: folderName,
    }
  })
}
export function updateFolder(id, folderName) {
  return apiService.put(`/folders/${id}/`, {
  data: {
    name: folderName,
  }
})}
export function destroyFolder(id) {return apiService.delete(`/folders/${id}`)}
export function updateFolderEmailReporterState(folderId, state) {
  return apiService.patch(`/folders/${folderId}/`, {
    data: {
      email_notifications: state
    }
  })
}


// Handles actions
export function createHandle(twitterLink, folderId) {
  return apiService.post('/handles/', {
    data: {
      twitter_link: twitterLink,
      folder: folderId,
    }
  })
}
export function updateHandle(id, twitterLink, folderId ){
  return apiService.put(`/handles/${id}/`, {
    data: {
      twitter_link: twitterLink,
      folder: folderId,
    }
  })
}
export function destroyHandle(id) {return apiService.delete(`/handles/${id}`)}


//Tweets actions
export function getTweets(handleId, dateTimeStart, dateTimeEnd) {
  let requestedIds = null;

  if (typeof(handleId) === "number") {
    requestedIds = handleId
  }

  if (typeof(handleId) === "object") {
    requestedIds = handleId.join(',')
  }

  return apiService.get(`/tweets/?dtStart=${dateTimeStart}&dtEnd=${dateTimeEnd}&ids=${requestedIds}`)
}


// Mail reporters actions
export function updateMailReporter(mailReportId, timeOffset, rateLimit, emailList, columnToUse, frequency) {
  return apiService.put(`/mailreporters/${mailReportId}/`, {
    data: {
      time_offset: timeOffset,
      rate_limit: rateLimit,
      emails_to_report: emailList,
      column_to_use: columnToUse,
      frequency: frequency
    }
  })
}
export function destroyEmail(id) {
  return apiService.delete(`/emails/${id}`)
}


// Actions with files
export function downloadFollowings(twitterLink) {
  return apiService.post('/downloads/', {
    twitter_link: twitterLink,
  })
}
export function uploadHandles(folderId, file) {
  const formData = new FormData();
  formData.append('file', file);
  formData.append('folder_id', folderId);

  return apiService.post('/uploads/',
    formData)
}
